
public class AutoTest {

	public static void main(String[] args) {
		Auto a1;
		
		a1 = new Auto();
		
		a1.setHersteller("BMW");
		System.out.println(a1.getHersteller());
		a1.setModell("e500");
		System.out.println(a1.getModel());
		Auto a2 = new Auto( "VW", "Golf");
		
		System.out.println(a2.getHersteller());
		System.out.println(a2.getModel());
		System.out.println(a2.toString());
	
	}

}
