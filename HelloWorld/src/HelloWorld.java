
public class HelloWorld {

	public static void main(String[] args) {
		System.out.println("Hallo Welt!");
		System.out.printf("Hello %s!%n", "World");
		//das ln bei println() steht f�r line=Zeile. Das hei�t, dass die neue Ausgabe in einer neuen Zeile erfolgen wird, also mit einem Zeilenumbruch/Absatz. Als ob du die Enter-Taste gedr�ckt h�ttest, Bei print() Wird in der aktuellen Zeile weitergeschrieben.
		System.out.println("        *");
		System.out.println("       ***");
		System.out.println("      *****");
		System.out.println("     *******");
		System.out.println("    *********");
		System.out.println("   ***********");
		System.out.println("  *************");
		System.out.println("       ***");
		System.out.println("       ***");
		double d = 22.4234234;
		double v = 111.2222;
		double u = 4.0;
		double r = 1000000.551;
		double t = 97.34;
	
		System.out.printf( "%10.2f%n",d );
		
		System.out.printf( "%10.2f%n",v);
		
		System.out.printf( "%10.2f%n",u);
	
		System.out.printf( "%10.2f%n",r);
		
		System.out.printf( "%10.2f",t);
		
		
		
		
	}

}
