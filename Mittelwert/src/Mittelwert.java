
public class Mittelwert {

	public static void main(String[] args) {
		// (E) "Eingabe"
		// Werte f�r x und y festlegen:
		// ===========================
		double x = 2.0;
		double y = 4.0;
		double ergebnis = berechneMittelwert(x,y);
		System.out.print(ergebnis);
	}

	// (V) Verarbeitung
	// Mittelwert von x und y berechnen:
	// ================================

	// (A) Ausgabe
	// Ergebnis auf der Konsole ausgeben:
	// =================================

	public static double berechneMittelwert(double x, double y) {
		double mittelwert;
		mittelwert = (x + y) / 2.0;
		return mittelwert;
	}
	 

}
