﻿import java.util.Scanner;

class Fahrkartenautomat {
	private static double rückgabebetrag;

	public static void main(String[] args) {
		double zuZahlenderBetrag = fahrkartenbestellungErfassen();
		double rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
		fahrkartenAusgeben();
		rückgeldAusgeben(rückgabebetrag);

	}

	public static double fahrkartenbestellungErfassen() {
		Scanner tastatur = new Scanner(System.in);
		int wunschFahrkarte = 0;// für Übersicht erstellt
		int ticketAnzahl;// als int erstellt, damit nur ganze Zahlen eingegeben werden können
		double zuZahlenderBetrag = 0; // Berechnung des Gesamtbetrages
		double ticketPreis1 = 2.90;
		double ticketPreis2 = 8.60;
		double ticketPreis3 = 23.50;
		double zwischenSumme = 0;

		System.out.println("Fahrkartenbestellvorgang:");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		}
while(wunschFahrkarte != 9) {
	System.out.println("\nWählen Sie ihre Wunschfahrkarte für Berlin AB aus:");
	System.out.println("Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\r\n" + "Tageskarte Regeltarif AB [8,60 EUR] (2)\r\n"
					+ "Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)\r\n"
			        + "Bezahlen (9)");
	System.out.print("Ihre Wahl: ");
	wunschFahrkarte = tastatur.nextInt();
	while (wunschFahrkarte > 3 && wunschFahrkarte != 9) {
		System.out.println(">>falsche Eingabe<<");
		System.out.print("Ihre Wahl: ");
		wunschFahrkarte = tastatur.nextInt();

	}
	if (wunschFahrkarte == 9) {
		zuZahlenderBetrag = zuZahlenderBetrag + zwischenSumme;
		return zuZahlenderBetrag;
	}

	System.out.print("Anzahl der Tickets: ");
	ticketAnzahl = tastatur.nextInt();
	while (ticketAnzahl > 10 || ticketAnzahl < 1) {
		System.out.println("Wählen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
		System.out.print("Anzahl der Tickets: ");
		ticketAnzahl = tastatur.nextInt();
	}
	if (wunschFahrkarte == 1) {
		zwischenSumme = ticketAnzahl * ticketPreis1 + zwischenSumme;
		System.out.println("Zwischensumme: " + zwischenSumme + " €");


	}
	if (wunschFahrkarte == 2) {
		 zwischenSumme = ticketAnzahl * ticketPreis2 + zwischenSumme;
		System.out.println("Zwischensumme: " + zwischenSumme + " €");
	}
	if (wunschFahrkarte == 3) {
		zwischenSumme = ticketAnzahl * ticketPreis3 + zwischenSumme;
		System.out.println("Zwischensumme: " + zwischenSumme + " €");
	}
	
}
   
zuZahlenderBetrag = zuZahlenderBetrag + zwischenSumme;
		return zuZahlenderBetrag;

	}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		double eingezahlterGesamtbetrag = 0.0;

		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			Scanner tastatur = new Scanner(System.in);

			double eingeworfeneMünze;

			System.out.printf("Noch zu zahlen: " + "%.2f Euro \n", zuZahlenderBetrag - eingezahlterGesamtbetrag);
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag = eingezahlterGesamtbetrag + eingeworfeneMünze;
		}
		double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		return rückgabebetrag;

	}

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		}

		System.out.println("\n\n");
	}

	public static void rückgeldAusgeben(double rückgabebetrag) {

		if (rückgabebetrag > 0.0) {
			System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				System.out.println("2 EURO");
				rückgabebetrag -= 2.0;
			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				System.out.println("1 EURO");
				rückgabebetrag -= 1.0;
			}
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				System.out.println("50 CENT");
				rückgabebetrag -= 0.5;
			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				System.out.println("20 CENT");
				rückgabebetrag -= 0.2;
			}
			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				System.out.println("10 CENT");
				rückgabebetrag -= 0.1;
			}
			while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				System.out.println("5 CENT");
				rückgabebetrag -= 0.05;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.");

	}

}