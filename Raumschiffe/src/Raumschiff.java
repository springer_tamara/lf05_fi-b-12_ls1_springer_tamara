import java.util.ArrayList;

public class Raumschiff {
	private String schiffsname;
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private ArrayList<String> broadcastKommunikator;
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();

	public Raumschiff() {
		System.out.println("Standart");
	};

	public ArrayList<Ladung> getLadungsverzeichnis() {
		return ladungsverzeichnis;
	}

	public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}

	public Raumschiff(String schiffsname, int photonentorpedoAnzahl, int energieversorgungInProzent,
			int schildeInProzent, int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl,
			ArrayList<String> broadcastKommunikator, ArrayList<Ladung> ladungsverzeichnis) {
		super();
		this.androidenAnzahl = androidenAnzahl;
		this.broadcastKommunikator = broadcastKommunikator;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.ladungsverzeichnis = ladungsverzeichnis;
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.schiffsname = schiffsname;
		this.schildeInProzent = schildeInProzent;
	}

	public String getSchiffsname() {
		return schiffsname;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}

	public void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		this.broadcastKommunikator = broadcastKommunikator;
	}

	public void addLadung(Ladung l1) {
		this.ladungsverzeichnis.add(l1);
	}

	public void photonentorpedoSchiessen(Raumschiff r1) {

	}

	public void phaserkanoneSchiessen(Raumschiff r1) {

	}

	private void treffer(Raumschiff r1) {

	}

	public void nachrichtAnAlle(String message) {

	}
	 ArrayList<String>  eintraegeLogbuch;
	public ArrayList<String> eintraegeLogbuchZurueckgeben() {
		return eintraegeLogbuch;

	}

}
